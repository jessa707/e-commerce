const Order = require("../models/Order");
const User = require('../models/User')
const Product = require('../models/Product')


function adminOnleMiddleware(request, response, next) {
  const token = request.headers.authorization;
  const user = auth.decode(token);

  if (user.isAdmin) {
    next();
  } else {
    response.status(401).send("You are not an admin");
  }
}

// ================= OPTION 1 =============================

// Should be created immediately after registering
// module.exports.instantiateOrder = (userId) => {
//   return Order.create({userId: userId,}).then((result) => {
//     return result;
//   });
// };

// //Call on creating an order
// module.exports.addOrder = (userId, productId, amount) => {
//   return Order.findOneAndUpdate(
//     {

//       userId: userId,
//     },
//     {
//       $push: {
//         products: {
//           productId: productId,
//           quantity: amount,
//         },
//       },
//       $inc: {
//         totalAmount: amount 
//       },
//     }
//   ).then((result) => {
//     return {
//       message: 'Your order was created'
//     }
//   });
// };


// ================= OPTION 2 =============================

// module.exports.createOrder = (data) => {
//   // if(data.isAdmin) {
//   //   return Promise.resolve ({
//   //     message: "This is an ADMIN account, invalid request."
//   //   })
//    let new_order = new Order ({
//     userId: data.userId,
//     products: [{
//       productId: data.products.productId,
//       quantity: data.products.quantity,
//       price: data.products.price
//     }],
//     totalAmount: data.totalAmount,
//   }) 
//    console.log(data)

//   return new_order.save().then((updated_order, error) => {
//       if(error){
//         return false
//       }

//     return ({
//       message: 'Successful order created.'
//     }) 

//   })
// }

// ==================================================================================


//======================== OPTION 3 ==================================

module.exports.createOrder  = async (data, userId, newOrder) => {
        if(data.isAdmin){
           let message = Promise.resolve({
                message: 'Admin cannot place an order.'
            })
            return message.then((value) => {
                return value
            })
        }

        const new_order = new Order({
        userId: userId,
        products: newOrder.products
        // totalAmount: await totalAmount(newOrder)

        })
        return new_order.save().then((saved_newOrder) => {
            if(saved_newOrder !== null){
                return {
                    message: "Order has been successfully placed!"
                    }
                }       
            })
        }

        // const totalAmount =  async (newOrder) => {
        //     if(newOrder.products.length > 0) {
        //         let subTotalAmount = 0

        //     for(let index = 0; index < newOrder.products.length; index++){
            
        //     const price = await Product.findById(newOrder.products[index].productId).then((product) => {
        //         if(product !=null){
        //             return product.price
        //             } return 0}).catch((error) => {
        //             return 0
        //         })
        //         subTotalAmount += price * newOrder.products[index].quantity
        //         }
        //         return subTotalAmount
        //     }
        // }




module.exports.showAllOrders = () => {
    return Order.find({}).then((result, error) => {
        if(error){
        return false
        }
        return result
  })
}

// module.exports.userOrders = (userId) => {

//         return Order.find({userId: userId}).then(result => {
//             if(result.length > 0){
//                 return result
//             }
//             return {
//                 message: "No orders found!"
//             }
//         })
//     }



// // Get list of orders
// module.exports.getOrders = (userId) => {
//   return Order.find({
//     userId: userId,
//   }).then((result) => {
//     return result;
//   });
// };
