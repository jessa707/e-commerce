const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkIfEmailExists = (data) => {
  return User.find({email: data.email}).then((result) => {
    if(result.length > 0){
      return true
    }

    return false
  })
}

// User Registration Controller
module.exports.register = (data) => {
  let encypted_password = bcrypt.hashSync(data.password, 10);

  let new_user = new User({
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
    mobileNo: data.mobileNo,
    password: encypted_password
    
  });

  return new_user.save().then((created_user, error) => {
    if (error) {
      return false;
    }

    return {
      message: 'User successfully registered!'
    }
  });
};

// User login Controller
module.exports.login = (data) => {
  return User.findOne({ email: data.email }).then((result) => {
    if (result == null) {
      return {
        message: "User doesn't exist!",
      };
    }

    const is_password_correct = bcrypt.compareSync(
      data.password,
      result.password
    );

    if (is_password_correct) {
      return {
        accessToken: auth.createAccessToken(result),
      };
    }

    return {
      message: "Password is incorrect!",
    };
  });
};

module.exports.getProfile = (data) => {

  return User.findById(data.userId).then(result => {

    // Makes the password not be included in the result
    result.password = "";

    // Returns the user information with the password as an empty string
    return result;

  });
};

// User Get Details Controller
// module.exports.getUserDetails = () => {
//   return User.find().then((result) => {
//     return result;
//   });
// };