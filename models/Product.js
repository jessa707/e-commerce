const mongoose = require("mongoose");

const product_schema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "The Name of the product is required"],
    unique: true,
  },
  description: {
    type: String,
    required: [true, "Description is required"],
  },
  price: {
    type: Number,
    required: [true, "Price is required."],
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  createdOn: {
    type: Date,
    default: new Date(),
  },
});

module.exports = mongoose.model("Product", product_schema);
